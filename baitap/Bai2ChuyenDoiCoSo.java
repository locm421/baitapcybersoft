package baitap;
public class Bai2ChuyenDoiCoSo extends TinhTienKaraok {
	public static void main(String[] args)
	{
		System.out.print("Nhập cơ số 10: ");
		int n = sc.nextInt();
		convertToBi(n);
	}
	public static void convertToBi(int decimal)
	{
		int index = 1;
		int Binary = 0;
		while(decimal > 0)
		{
			Binary += index*(decimal % 2);
			decimal /= 2; 
			index *= 10;
		}
		System.out.println("Hệ cơ số 2: " + Binary);
	}
}

