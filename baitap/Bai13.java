package baitap;

import java.util.Scanner;

public class Bai13 {
	static int max = 0;
	static int min = 0;
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập số phần tử: ");
		int n = sc.nextInt();

		int[] arr = new int[n];
		System.out.println("Nhập mảng:");
		nhap(arr);
		System.out.println("Xuất mảng:");
		xuat(arr);
		trungBinh(arr);
		findMaxMin(arr);
		amLonBeNhat(arr);
		duongLonBeNhat(arr);
		xuatChanLe(arr);
		add(arr);
		xoa(arr);

	}

	public static void nhap(int[] arr) {

		for (int i = 0; i < arr.length; i++) {
			arr[i] = sc.nextInt();
		}
	}

	public static void xuat(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}

	public static void trungBinh(int[] arr) {
		double sum = 0;
		for (int i = 0; i < arr.length; i++) {
			sum += arr[i];
		}
		double avg = sum / arr.length;
		System.out.println("Giá trị trung bình là: " + avg);
	}

	public static void findMaxMin(int[] arr) {
		min = arr[0];
		max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > max)
				max = arr[i];
			if (arr[i] < min)
				min = arr[i];

		}
		System.out.println("Phần tử lớn nhất là: " + max);
		System.out.println("Phần tử bé nhất là: " + min);
	}

	public static void amLonBeNhat(int[] arr) {
		int maxN = min;
		if (min < 0) {
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] > maxN && arr[i] < 0) {
					maxN = arr[i];
				}
			}
			System.out.println("Số âm lớn nhất là: " + maxN);
			System.out.println("Số âm bé nhất là: " + min);
		} else {
			System.out.println("Không có số âm trong mảng");
		}

	}
	public static void duongLonBeNhat(int[] arr) {
		int minP = max;
		if (max > 0) {
			for (int i = 0; i < arr.length; i++) {
				if (arr[i] < minP && arr[i] > 0) {
					minP = arr[i];
				}
			}
			System.out.println("Số dương lớn nhất là: " + max);
			System.out.println("Số dương bé nhất là: " + minP);
		} else {
			System.out.println("Không có số dương trong mảng");
		}

	}
	public static void xuatChanLe(int[] arr) {
		int count = 0;
		System.out.print("Các phần tử chẳn là: ");
		for (int i = 0; i < arr.length; i++) {
			if(arr[i] % 2 == 0)
			{
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
		System.out.print("Các phần tử lẻ là: ");
		for (int i = 0; i < arr.length; i++) {
			if(arr[i] % 2 != 0)
			System.out.print(arr[i] + " ");
		}
		
	}
	public static void add(int[] arr) {
		System.out.println();
		int[] arrAdd = new int[arr.length + 1];
		for (int i = 0; i < arr.length; i++) {
			arrAdd[i] = arr[i];
        }
		System.out.print("Nhập 1 phần tử:");
		int a = sc.nextInt();
		System.out.print("Nhập vị trí phần tử cần thêm vào, vị trí bắt đầu từ 1:");
		int index = sc.nextInt();
		for (int i = arrAdd.length - 1; i >= index ; i--) {
			arrAdd[i] = arrAdd[i - 1];
		}
		System.out.print("Lỗi 1 ");
		arrAdd[index - 1] = a;
		System.out.print("Mảng sau khi thêm là: ");
		for(int i = 0; i < arrAdd.length; i++)
			System.out.print(arrAdd[i] + " ");
		System.out.println();
	}
	public static void xoa(int[] arr) {
		System.out.println();
		System.out.print("Nhập vị trí phần tử cần XOÁ vào, vị trí bắt đầu từ 1:");
		int index = sc.nextInt();
		for (int i = index - 1; i < arr.length - 1; i++) {
			arr[i] = arr[i + 1];
		}
		System.out.print("Mảng sau khi XOÁ là: ");
		for(int i = 0; i < arr.length - 1; i++)
			System.out.print(arr[i] + " ");
		System.out.println();
	}

}
