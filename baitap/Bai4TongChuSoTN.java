package baitap;

import java.util.Scanner;

public class Bai4TongChuSoTN {
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhập số tự nhiên: ");
		int n = sc.nextInt();
		int sum = 0;
		for(int i = 1; i<=n;i++)
			sum += i;
		System.out.println("Tổng của các số tự nhiên của " + n + " là: " + sum);
	}
}
