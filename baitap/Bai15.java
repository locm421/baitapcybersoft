package baitap;

import java.util.Scanner;

public class Bai15 {
	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		System.out.println("Nhập chuỗi: ");
		String str = sc.nextLine();
		System.out.println("Nhập index: ");
		int index = sc.nextInt();
		System.out.println("Ký tự tại vị trí " + index + " là: ");
		System.out.println(str.charAt(index-1));
		String subStr = "abcdef";
		 if (str.indexOf(subStr) != -1) {
	            System.out.println("Chuỗi con '" + subStr + "' nằm trong chuỗi '" + str + "'.");
	        } else {
	            System.out.println("Chuỗi con '" + subStr + "' không nằm trong chuỗi '" + str + "'.");
	        }
	}
}
