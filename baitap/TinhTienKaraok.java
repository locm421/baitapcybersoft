package baitap;

import java.util.Scanner;
/*					KẾT QUẢ DEMO
 * 
 * đầu vào
 * 12 13
 * 13 12
 * 4
 * Thông tin hoá đơn
+--------------------+------------------+
| Thông tin dịch vụ  |      Đơn giá     |
----------------------------------------+
| Giờ hát            |         0.98 giờ |
----------------------------------------+
| Số chai nước: 4    |       40000 đồng |
----------------------------------------+
| Tổng giá           |       55600 đồng |
+--------------------+------------------+
| Vào cửa từ 9:00-17:00 đã được giảm 20%|
+---------------------------------------+
 * ====================
 * đầu vào 
 * 17 29 
 * 24 0
 * 5
 * Thông tin hoá đơn
+--------------------+------------------+
| Thông tin dịch vụ  |      Đơn giá     |
----------------------------------------+
| Giờ hát            |         6.51 giờ |
----------------------------------------+
| Số chai nước: 5    |       50000 đồng |
----------------------------------------+
| Tổng giá           |      171650 đồng |
+--------------------+------------------+
| Không được giảm giá                   |
+---------------------------------------+
 */

public class TinhTienKaraok {
	static Scanner sc = new Scanner(System.in);
	static int soChaiNuoc ;
	public static void main(String[] arg) {
		double temp;
		double timePlay = 0;
		int[] timeStart = new int[2];
		int[] timeEnd = new int[2];
		nhap(timeStart, timeEnd);
		timePlay = tinhTime(timeStart, timeEnd);
		do
		{
		System.out.print("Nhập số chai nước: ");
		soChaiNuoc = sc.nextInt();
		}
		while(soChaiNuoc < 0);
		temp = tongGia(timePlay,timeEnd, soChaiNuoc);
		xuatBill(temp, soChaiNuoc, timePlay, timeEnd[0]);
	}
	public static void nhap(int[] timeStart,int[] timeEnd)
	{
		System.out.println("Nhập thời gian BẮT ĐẦU: ");
		do
		{
			System.out.println("Giờ mở của từ 9:00-24:00 và phút bé hơn 60 lớn hơn 0. Vui lòng nhập time hợp lệ !");
			System.out.print("Nhập giờ: ");
			timeStart[0] = sc.nextInt();
			System.out.print("Nhập phút: ");
			timeStart[1] = sc.nextInt();
			
		}while(timeStart[0] < 9 || timeStart[0] > 24 || timeStart[1] > 60 ||timeStart[1] < 0);
		System.out.println("=========================");
		System.out.println("Nhập thời gian KẾT THÚC: ");
		do
		{
			System.out.println("Giờ kết thúc từ 9:00-24:00 và phút bé hơn 60 lớn hơn 0. Vui lòng nhập time hợp lệ !");
			System.out.print("Nhập giờ: ");
			timeEnd[0] = sc.nextInt();
			System.out.print("Nhập phút: ");
			timeEnd[1] = sc.nextInt();
		}while(timeEnd[0] < 9 || timeEnd[0] > 24 || timeEnd[1] > 60 ||timeEnd[1] < 0 || timeEnd[0] < timeStart[0]) ;
	}
	
	public static double tinhTime(int[] timeS, int[] timeE)
	{
		double time = (double)((timeE[0]*60+timeE[1] - (timeS[0]*60+timeS[1])))/60;
		return time;
	}
	
	public static double tongGia(double timePlay, int[] timeEnd, int soChaiNuoc)
	{
		double gia = 0; 
		double giaSau = 0;
		double tongBill;
		if(timePlay <= 3)
			gia = timePlay * 30000;
		if(timePlay > 3)
		{
			giaSau = 90000+(timePlay - 3)*(30000*30/100);
		}
		tongBill = (double) (gia + giaSau + soChaiNuoc * 10000);
		if(timeEnd[0] < 17)
			tongBill = (double)(gia + giaSau + soChaiNuoc * 10000)* 80/100;
		return tongBill;
	}
	public static void xuatBill(double tongGia, int soChaiNuoc,double timePlay, int timeE)
	{
		System.out.println("Thông tin hoá đơn");
		System.out.println("+--------------------+------------------+");
		System.out.println("| Thông tin dịch vụ  |      Đơn giá     |");
		System.out.println("----------------------------------------+");
		System.out.printf("| %-18s | %16s |\n", "Giờ hát", (double) Math.floor(timePlay * 100) / 100 + " giờ");
		System.out.println("----------------------------------------+");
		System.out.printf("| %-18s | %16s |\n", "Số chai nước: " + soChaiNuoc,  soChaiNuoc* 10000 + " đồng");
		System.out.println("----------------------------------------+");
		System.out.printf("| %-18s | %16s |\n", "Tổng giá", (int)tongGia + " đồng");
		System.out.println("+--------------------+------------------+");
		if(timeE < 17)
			System.out.printf("| %-34s|\n", "Vào cửa từ 9:00-17:00 đã được giảm 20%");
		else
			System.out.printf("| %-38s|\n", "Không được giảm giá");
		System.out.println("+---------------------------------------+");
	}
}
