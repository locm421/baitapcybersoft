package baitap;

import java.util.Scanner;

public class Bai10 {
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Nhập vào một số tự nhiên n: ");
        int n = sc.nextInt();
        
        int sum = findPrime(n);
        System.out.println("Tổng các số nguyên tố từ 1 đến " + n + " là: " + sum);
    }
    
    public static int findPrime(int n) {
        int sum = 0;
        
        for (int i = 2; i <= n; i++) {
            if (isPrime(i)) {
                sum += i;
            }
        }
        
        return sum;
    }
    
    public static boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }
        
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0) {
                return false;
            }
        }
        
        return true;
    }
}
