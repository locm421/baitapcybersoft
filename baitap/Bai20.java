package baitap;

import java.util.Scanner;

public class Bai20 {
	static Scanner sc = new Scanner(System.in);
	public static void main(String[] args)
	{
		int n;
		System.out.println("Nhập kích thước mảng: ");
		n = sc.nextInt();
		int arr[] = new int[n];
		nhap(arr);
		System.out.print("Mảng vừa nhập: ");
		xuat(arr);
		chiaMang(arr);
		
	}
	public static void nhap(int[] arr) {

		for (int i = 0; i < arr.length; i++) {
			arr[i] = sc.nextInt();
		}
	}

	public static void xuat(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
	}
	public static void chiaMang(int[] arr) {
		int count = 0;
		for (int i = 0; i < arr.length; i++) {
			if(arr[i] % 2 == 0)
				count++;
		}
		int arrChan[] = new int[count];
		int arrLe[] = new int[arr.length-count];
		int indexChan = 0;
		int indexLe = 0;
		for(int i = 0; i < arr.length ; i++)
		{
			if(arr[i] % 2 == 0)
			{
				arrChan[indexChan] = arr[i];
				indexChan++;
			}
			else
			{
				arrLe[indexLe] = arr[i];
				indexLe++;
			}
		}
		System.out.print("Mảng chẳn: ");
		xuat(arrChan);
		System.out.print("Mảng lẻ: ");
		xuat(arrLe);
	}
}
