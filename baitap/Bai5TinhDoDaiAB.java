package baitap;

import java.util.Scanner;

public class Bai5TinhDoDaiAB {
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhập toạ độ xA: ");
		int xA = sc.nextInt();
		System.out.print("Nhập toạ độ yA: ");
		int yA = sc.nextInt();
		System.out.print("Nhập toạ độ xB: ");
		int xB = sc.nextInt();
		System.out.print("Nhập toạ độ yB: ");
		int yB = sc.nextInt();
		double AB = Math.sqrt(Math.pow(xA-xB, 2) + Math.pow(yA-yB, 2)); 
		System.out.println("Độ dài đoạn thẳng AB là: " + AB);
	}
}
