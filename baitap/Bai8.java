package baitap;

import java.util.Scanner;

public class Bai8 {
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Nhập vào một chuỗi viết thường: ");
        String chuoi = sc.nextLine();
        
        String vietHoa = capital(chuoi);
        System.out.println("Chuỗi sau khi chuyển đổi: " + vietHoa);
    }
    
    public static String capital(String chuoi) {
        StringBuilder result = new StringBuilder();
        String[] words = chuoi.split("\\s");
        
        for (String word : words) {
            char firstChar = Character.toUpperCase(word.charAt(0));
            String restOfString = word.substring(1);
            String capitalizedWord = firstChar + restOfString;
            result.append(capitalizedWord).append(" ");
        }
        
        return result.toString().trim();
    }
}
