package baitap;

import java.util.Scanner;

public class Bai19 {
	 public static void main(String[] args) {
	 Scanner scanner = new Scanner(System.in);
     int choice;

     do {
         System.out.println("----- MENU -----");
         System.out.println("1. Giải phương trình bậc 1");
         System.out.println("2. Giải phương trình bậc 2");
         System.out.println("3. Thoát");
         System.out.print("Nhập lựa chọn của bạn: ");
         choice = scanner.nextInt();

         switch (choice) {
             case 1:
                 bacMot();
                 break;
             case 2:
                 bacHai();
                 break;
             case 3:
                 System.out.println("Đã thoát chương trình.");
                 break;
             default:
                 System.out.println("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                 break;
         }
     } while (choice != 3);

}
 public static void bacMot() {
     Scanner scanner = new Scanner(System.in);
     System.out.println("----- GIẢI PHƯƠNG TRÌNH BẬC 1 -----");
     System.out.print("Nhập hệ số a: ");
     double a = scanner.nextDouble();
     System.out.print("Nhập hệ số b: ");
     double b = scanner.nextDouble();

     if (a == 0) {
         if (b == 0) {
             System.out.println("Phương trình vô số nghiệm.");
         } else {
             System.out.println("Phương trình vô nghiệm.");
         }
     } else {
         double x = -b / a;
         System.out.println("Nghiệm của phương trình là: " + x);
     }
 }

 public static void bacHai() {
     Scanner scanner = new Scanner(System.in);
     System.out.println("----- GIẢI PHƯƠNG TRÌNH BẬC 2 -----");
     System.out.print("Nhập hệ số a: ");
     double a = scanner.nextDouble();
     System.out.print("Nhập hệ số b: ");
     double b = scanner.nextDouble();
     System.out.print("Nhập hệ số c: ");
     double c = scanner.nextDouble();

     if (a == 0) {
         bacMot();
     } else {
         double delta = b * b - 4 * a * c;

         if (delta < 0) {
             System.out.println("Phương trình vô nghiệm.");
         } else if (delta == 0) {
             double x = -b / (2 * a);
             System.out.println("Phương trình có nghiệm kép: " + x);
         } else {
             double x1 = (-b + Math.sqrt(delta)) / (2 * a);
             double x2 = (-b - Math.sqrt(delta)) / (2 * a);
             System.out.println("Phương trình có hai nghiệm phân biệt:");
             System.out.println("Nghiệm thứ nhất: " + x1);
             System.out.println("Nghiệm thứ hai: " + x2);
         }
     }
 }
}
