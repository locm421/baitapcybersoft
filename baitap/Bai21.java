package baitap;

import java.util.Scanner;

public class Bai21 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhập địa chỉ của kan1 : ");
		int x1 = sc.nextInt();
		System.out.print("Nhập vận tốc của kn1: ");
		int v1 = sc.nextInt();
		System.out.println("=============");
		System.out.print("Nhập địa chỉ của kan2 : ");
		int x2 = sc.nextInt();
		System.out.print("Nhập vận tốc của kn2: ");
		int v2 = sc.nextInt();
		int m = x1 - x2;
		int n = v1 - v2;
		if (v1 == v2 && x1 != x2)
			System.out.println("Không gặp nhau");
		else if (v1 == v2 && x1 == x2)
			System.out.println("Gặp nhau tại: " + x1);
		else {
			if (m % n == 0) {
				if (-(m / n) < 0)
					System.out.println("Không gặp nhau");
				else
					System.out.println("Gặp nhau tại vị trí: " + (x1 + v1 * Math.abs(m / n)));
			} else
				System.out.println("Không gặp nhau");
		}
	}

}
